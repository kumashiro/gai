/*
 * help.c  - Help functions.
 *
 * This file is part of gai project.
 *
 * gai is  free software: you  can redistribute it  and/or modify it  under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * gai is  distributed in  the hope  that it  will be  useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * gai. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <stdio.h>

#include "config.h"
#include "gettext.h"
#include "help.h"


void  help_print(FILE  *stream)
{
    fprintf(stream, _(
        "Usage:\n\n"
        "  %s [ -46fhV ] [ [-P|-p] PREFIX ] [ [-S|-s] SUFFIX ] [ [-D|-d] DELIMITER ]\n"
        "       [ NAME ... | -r ADDRESS ... ]\n\n"
        "  -4, --ipv4                  return IPv4 adresses only (if any)\n"
        "  -6, --ipv6                  return IPv6 adresses only (if any)\n"
        "  -D DELIMITER, --delimiter DELIMITER\n"
        "                              separate results with DELIMITER, interpret escapes\n"
        "  -d DELIMITER, --literal-delimiter DELIMITER\n"
        "                              separate results with literal DELIMITER\n"
        "  -f, --first                 print only first result for every NAME\n"
        "  -h, --help                  print this help screen and exit\n"
        "  -r, --reverse               resolve IP address(es) to name(s)\n"
        "  -P PREFIX, --prefix PREFIX  prepend PREFIX to each result, interpret escapes\n"
        "  -p PREFIX, --literal-prefix PREFIX\n"
        "                              prepend PREFIX to each result literally\n"
        "  -S SUFFIX, --suffix SUFFIX  append SUFFIX to each result, interpret escapes\n"
        "  -s SUFFIX, --literal-suffix SUFFIX\n"
        "                              append SUFFIX to each result literally\n"
        "  -V, --version               print program name, version and exit\n\n"
        "  NAME                        name to resolve\n"
        "  ADDRESS                     IP address to resolve\n\n"
        "NOTE: long options may not be available on some platforms\n"),
        PACKAGE_NAME
    );
}


inline
void  help_print_version(FILE  *stream)
{
    fprintf(stream,
            PACKAGE_NAME " " GAI_VERSION "\n"
            "Copyright (C) 2016 Kaito Kumashiro\n"
            "This program comes with ABSOLUTELY NO WARRANTY.\n"
            "This is free software, licensed under the terms of the GNU General Public License.\n"
            "For details, please see the LICENSE.md file that came with this program or point\n"
            "your browser to the web address https://www.gnu.org/licenses/gpl-3.0.en.html on the\n"
            "Internet, SkyNet, Galnet or whatever is available at your time.\n");
}

/* vim: set ft=c sw=4 sts=4 et: */
