#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif /* _POSIX_SOURCE */

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE     200112L
#endif /* _POSIX_C_SOURCE */

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE       700
#endif /* _XOPEN_SOURCE */

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "config.h"

#ifdef LIBIDN
#include <idna.h>
#include <stringprep.h>
#endif /* LIBIDN */

#include "gettext.h"
#include "resolver.h"

#ifndef NI_MAXHOST
#define NI_MAXHOST          1025
#endif

#ifndef INET6_ADDRSTRLEN
#define INET6_ADDRSTRLEN    46
#endif /* INET6_ADDRSTRLEN */


typedef struct {
    int     sys_error;
    int     gai_error;
    int     idn_error;
} Errors;


static
void  print_delimiter(Settings  *settings)
{
    static int      first = 1;


    if ( first == 1 ) {
        first = 0;
    } else if ( settings->delimiter != NULL ) {
        printf("%s", settings->delimiter);
    } else {
        printf("\n");
    };
}


static inline
void  print_single_result(Settings  *settings, char * const  result)
{
    print_delimiter(settings);
    printf("%s%s%s",
           settings->prefix == NULL ? "" : settings->prefix,
           result,
           settings->suffix == NULL ? "" : settings->suffix);
}


static inline
void  print_error(char * const  string, Errors  *errs)
{
    const char      *msg;


    if ( errs->sys_error != 0 )
        msg = strerror(errs->sys_error);
    else if ( errs->gai_error != 0 )
        msg = gai_strerror(errs->gai_error);
    else if ( errs->idn_error != 0 )
#ifdef LIBIDN
        msg = idna_strerror(errs->idn_error);
#else
        msg = "Unspecified error";
#endif /* LIBIDN */
    else
        return;

    fprintf(stderr, _("Error resolving `%s': %s\n"), string, msg);
}


static
int  print_addrs_from_name(Settings  *settings, char * const  name, struct addrinfo  *filter, Errors  *errs)
{
    struct addrinfo     *result, *results;
    struct sockaddr_in  *addr4;
    struct sockaddr_in6 *addr6;
    char                ip[INET6_ADDRSTRLEN];
    int                 code;


    results = NULL;
    memset(errs, 0, sizeof(Errors));
    if ( (code = getaddrinfo(name, NULL, filter, &results)) != 0 ) {
        if ( code == EAI_SYSTEM )
            errs->sys_error = errno;
        else
            errs->gai_error = code;

        return 1;
    };

    for ( result = results; result != NULL; result = result->ai_next ) {
        memset(ip, 0, sizeof(ip));
        if ( result->ai_family == AF_INET ) {
            addr4 = (struct sockaddr_in*)(result->ai_addr);
            code = inet_ntop(AF_INET, &(addr4->sin_addr), ip, sizeof(ip)) == NULL;
        } else if ( result->ai_family == AF_INET6 ) {
            addr6 = (struct sockaddr_in6*)(result->ai_addr);
            code = inet_ntop(AF_INET6, &(addr6->sin6_addr), ip, sizeof(ip)) == NULL;
        } else {
            continue;
        };

        if ( code != 0 ) {
            errs->sys_error = errno;
            break;
        } else {
            print_single_result(settings, ip);
        };

        if ( settings->flags & ARG_FLAG_FIRST )
            break;
    };

    freeaddrinfo(results);
    return errs->sys_error;
}


static
int  print_name_from_addr(Settings  *settings, char * const  address, Errors  *errs)
{
    struct sockaddr     *addr;
    struct sockaddr_in  addr4;
    struct sockaddr_in6 addr6;
    socklen_t           len;
    int                 code;
    char                hostname[NI_MAXHOST], *idn_hostname;


    memset(errs, 0, sizeof(Errors));
    if ( strchr(address, ':') == NULL ) {
        if ( !(settings->addr_type & ADDR_TYPE_IPV4) )
            return 0;
        len = sizeof(struct sockaddr_in);
        memset(&addr4, 0, len);
        addr4.sin_family = AF_INET;
        code = inet_pton(AF_INET, address, &(addr4.sin_addr));
        addr = (struct sockaddr*)&addr4;
    } else {
        if ( !(settings->addr_type & ADDR_TYPE_IPV6) )
            return 0;
        len = sizeof(struct sockaddr_in6);
        memset(&addr6, 0, len);
        addr6.sin6_family = AF_INET6;
        code = inet_pton(AF_INET6, address, &(addr6.sin6_addr));
        addr = (struct sockaddr*)&addr6;
    };

    if ( code == 0 ) {
        print_single_result(settings, address);
        return 0;
    } else if ( code < 0 ) {
        errs->sys_error = errno;
        return errno;
    };

    if ( (code = getnameinfo(addr, len, hostname, NI_MAXHOST, NULL, 0, NI_NAMEREQD)) != 0 ) {
        if ( code == EAI_SYSTEM )
            errs->sys_error = errno;
        else
            errs->gai_error = code;

        return 1;
    };

#ifdef LIBIDN
    if ( (code = idna_to_unicode_lzlz(hostname, &idn_hostname, 0)) != IDNA_SUCCESS ) {
        fprintf(stderr, _("IDN transformation failed: %s\n"), idna_strerror(code));
        errs->idn_error = code;
        return code;
    };
#else
    idn_hostname = hostname;
#endif /* LIBIDN */

    print_single_result(settings, idn_hostname);

#ifdef LIBIDN
    free(idn_hostname);
#endif /* LIBIDN */
    return 0;
}


int  resolve_names(Settings  *settings)
{
    struct addrinfo     filter;
    int                 code, i, have_results;
    char                hostname[NI_MAXHOST], *name, *idn_name;
    Errors              errs = {0, 0, 0};


    memset(&filter, 0, sizeof(struct addrinfo));
    if ( settings->addr_type == ADDR_TYPE_BOTH )
        filter.ai_family = AF_UNSPEC;
    else
        filter.ai_family = settings->addr_type == ADDR_TYPE_IPV4 ? AF_INET : AF_INET6;

    filter.ai_socktype = SOCK_DGRAM;
    filter.ai_flags = AI_PASSIVE;

    if ( settings->names[0] == NULL ) {
        if ( gethostname(hostname, NI_MAXHOST) != 0 ) {
            perror("gethostname()");
            return 1;
        } else {
            settings->names[0] = hostname;
        };
    };

    have_results = 0;
    for ( i = 0, code = 0; (name = settings->names[i]) != NULL; i++ ) {
#ifdef LIBIDN
        if ( (code = idna_to_ascii_lz(name, &idn_name, 0)) != IDNA_SUCCESS) {
            fprintf(stderr, _("IDN transformation failed: %s\n"), idna_strerror(code));
            continue;
        };
#else
        idn_name = (char*)name;
#endif /* LIBIDN */

        if ( (code = print_addrs_from_name(settings, idn_name, &filter, &errs)) != 0 )
            print_error(idn_name, &errs);
        else
            have_results = 1;

#ifdef LIBIDN
        free(idn_name);
#endif /* LIBIDN */
    };

    if ( have_results == 1 && (settings->delimiter == NULL || \
        (settings->delimiter != NULL && settings->delimiter[strlen(settings->delimiter)] != '\n')) )
        printf("\n");

    return 0;
}


int  resolve_adresses(Settings  *settings)
{
    int             code, i, have_results;
    char            hostname[NI_MAXHOST], *address;
    Errors          errs = {0, 0, 0};


    if ( settings->names[0] == NULL ) {
        if ( gethostname(hostname, NI_MAXHOST) != 0 ) {
            perror("gethostname()");
            return 1;
        } else {
            print_single_result(settings, hostname);
            return 0;
        };
    };

    have_results = 0;
    for ( i = 0, code = 0; (address = settings->names[i]) != NULL; i++ )
        if ( (code = print_name_from_addr(settings, address, &errs)) != 0 )
            print_error(address, &errs);
        else
            have_results = 1;

    if ( have_results == 1 && (settings->delimiter == NULL || \
        (settings->delimiter != NULL && settings->delimiter[strlen(settings->delimiter)] != '\n')) )
        printf("\n");

    return 0;
}

/* vim: set ft=c sw=4 sts=4 et: */
