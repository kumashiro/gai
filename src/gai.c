/*
 * gai.c  - Resolve host names to IP addresses.
 *
 * This file is part of gai project.
 *
 * gai is  free software: you  can redistribute it  and/or modify it  under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * gai is  distributed in  the hope  that it  will be  useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * gai. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <locale.h>
#include <stdio.h>
#include <string.h>

#include "config.h"

#include "gettext.h"
#include "help.h"
#include "resolver.h"
#include "settings.h"


static
char  *filename_from_path(char  *path)
{
    char    *fname;


    fname = strrchr(path, '/');
    if ( fname == NULL )
        fname = path;
    else
        fname++;

    return fname;
}


int  main(int  argc, char * const  argv[])
{
    Settings            *settings;
    int                 err, code;
    char                *callsign;


    setlocale(LC_ALL, "");
#ifdef GETTEXT
    bindtextdomain(PACKAGE_NAME, LOCALEDIR);
    textdomain(PACKAGE_NAME);
#endif /* GETTEXT */

    settings = settings_new(argc + 1);
    callsign = filename_from_path(argv[0]);
    if ( strcmp(callsign, "gai4") == 0 )
        settings->addr_type = ADDR_TYPE_IPV4;
    else if ( strcmp(callsign, "gai6") == 0 )
        settings->addr_type = ADDR_TYPE_IPV6;
    else if ( strcmp(callsign, "gni") == 0 )
        settings->flags |= ARG_FLAG_REVERSE;

    err = settings_parse_args(argc, argv, settings);
    if ( err != 0 || settings->flags & ARG_FLAG_HELP ) {
        help_print(err ? stderr : stdout);
        settings_free(settings);
        return err;
    };

    if ( settings->flags & ARG_FLAG_VERSION ) {
        help_print_version(stdout);
        settings_free(settings);
        return 0;
    };

    if ( settings->flags & ARG_FLAG_REVERSE )
        code = resolve_adresses(settings);
    else
        code = resolve_names(settings);

    settings_free(settings);
    return code < 0 ? code * -1 : code;
}

/* vim: set filetype=c expandtab tabstop=4 sts=4 shiftwidth=4: */
