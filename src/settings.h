/*
 * settings.h  - Settings and argument parsing functions (header file).
 *
 * This file is part of gai project.
 *
 * gai is  free software: you  can redistribute it  and/or modify it  under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * gai is  distributed in  the hope  that it  will be  useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * gai. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef SETTINGS_H
#define SETTINGS_H

typedef enum {
    ADDR_TYPE_DEFAULT = 0,
    ADDR_TYPE_IPV4 = 1,
    ADDR_TYPE_IPV6 = 2,
    ADDR_TYPE_BOTH = ADDR_TYPE_IPV4 | ADDR_TYPE_IPV6
} addr_type_t;


typedef enum {
    ARG_FLAG_NONE = 0,
    ARG_FLAG_HELP = 1,
    ARG_FLAG_VERSION = 2,
    ARG_FLAG_FIRST = 4,
    ARG_FLAG_REVERSE = 8,
    ARG_FLAG_LITERAL_PREFIX = 16,
    ARG_FLAG_LITERAL_SUFFIX = 32,
    ARG_FLAG_LITERAL_DELIMITER = 64
} setting_flag_t;


typedef struct {
    addr_type_t     addr_type;
    setting_flag_t  flags;
    char            *prefix;
    char            *suffix;
    char            *delimiter;
    char            **names;
} Settings;


int  settings_parse_args(int  argc, char * const  *argv, Settings  *settings);
Settings  *settings_new(int  names_length);
void  settings_free(Settings  *settings);

#endif /* SETTINGS_H */
/* vim: set ft=c sw=4 sts=4 et: */
