/*
 * settings.c  - Settings and argument parsing functions.
 *
 * This file is part of gai project.
 *
 * gai is  free software: you  can redistribute it  and/or modify it  under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * gai is  distributed in  the hope  that it  will be  useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * gai. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "gettext.h"
#include "settings.h"


#define ARGSPEC     "46D:d:fhP:p:rS:s:V"

#ifdef HAVE_GETOPT_LONG
static struct option    long_options[] = {
    {"delimiter",         required_argument, NULL, 'D'},
    {"literal-delimiter", required_argument, NULL, 'd'},
    {"first",                   no_argument, NULL, 'f'},
    {"first",                   no_argument, NULL, 'f'},
    {"help",                    no_argument, NULL, 'h'},
    {"ipv4",                    no_argument, NULL, '4'},
    {"ipv6",                    no_argument, NULL, '6'},
    {"prefix",            required_argument, NULL, 'P'},
    {"literal-prefix",    required_argument, NULL, 'p'},
    {"reverse",                 no_argument, NULL, 'r'},
    {"suffix",            required_argument, NULL, 'S'},
    {"literal-suffix",    required_argument, NULL, 's'},
    {"version",                 no_argument, NULL, 'V'},
    {NULL,                                0, NULL,   0}
};
#endif /* HAVE_GETOPT_LONG */


static
char  *unescape(char * const  escaped)
{
    char    *unescaped, *e, *u;


    /* TODO: support for numeric escape sequences */
    if ( escaped == NULL )
        return NULL;

    if ( (unescaped = (char*)malloc(strlen(escaped) * sizeof(char) + 1)) == NULL )
        abort();

    e = escaped;
    u = unescaped;
    while ( *e != '\0' ) {
        if ( *e == '\\' ) {
            switch ( *(++e) ) {
                case '\0': *u = '\0'; break;
                case '\\': *u = '\\'; break;
                case  'a': *u = '\a'; break;
                case  'b': *u = '\b'; break;
                case  'f': *u = '\f'; break;
                case  'n': *u = '\n'; break;
                case  'r': *u = '\r'; break;
                case  't': *u = '\t'; break;
                case  'v': *u = '\v'; break;
                default:
                    fprintf(stderr, _("Invalid escape sequence: \\%c\n"), *e);
                    free(unescaped);
                    return NULL;
            };

            if ( *e == '\0' )
                break;
        } else {
            *u = *e;
        };

        e++; u++;
    };

    return unescaped;
}


int  settings_parse_args(int  argc, char * const  *argv, Settings  *settings)
{
    int     arg;


    do {
#ifdef HAVE_GETOPT_LONG
        arg = getopt_long(argc, argv, ARGSPEC, long_options, NULL);
#else
        arg = getopt(argc, argv, ARGSPEC);
#endif /* HAVE_GETOPT_LONG */
        switch ( arg ) {
            case '4':
                settings->addr_type |= ADDR_TYPE_IPV4;
                break;
            case '6':
                settings->addr_type |= ADDR_TYPE_IPV6;
                break;
            case 'D':
                if ( (settings->delimiter = unescape(optarg)) == NULL )
                    return 1;
                break;
            case 'd':
                settings->flags |= ARG_FLAG_LITERAL_DELIMITER;
                settings->delimiter = optarg;
                break;
            case 'f':
                settings->flags |= ARG_FLAG_FIRST;
                break;
            case 'h':
                settings->flags |= ARG_FLAG_HELP;
                return 0;
            case 'P':
                if ( (settings->prefix = unescape(optarg)) == NULL )
                    return 1;
                break;
            case 'p':
                settings->prefix = optarg;
                settings->flags |= ARG_FLAG_LITERAL_PREFIX;
                break;
            case 'r':
                settings->flags |= ARG_FLAG_REVERSE;
                break;
            case 'S':
                if ( (settings->suffix = unescape(optarg)) == NULL )
                    return 1;
                break;
            case 's':
                settings->suffix = optarg;
                settings->flags |= ARG_FLAG_LITERAL_SUFFIX;
                break;
            case 'V':
                settings->flags |= ARG_FLAG_VERSION;
                return 0;
            case '?':
                return 1;
            default:
                break;
        };
    } while ( arg != -1 );

    for ( arg = 0; optind < argc; optind++, arg++ ) {
        settings->names[arg] = argv[optind];
    };

    if ( settings->addr_type == ADDR_TYPE_DEFAULT )
        settings->addr_type = ADDR_TYPE_BOTH;

    return 0;
}


Settings  *settings_new(int  names_length)
{
    Settings        *settings;


    if ( (settings = (Settings*)malloc(sizeof(Settings))) == NULL )
        abort();

    if ( (settings->names = (char**)malloc(names_length * sizeof(char*))) == NULL )
        abort();

    settings->addr_type = ADDR_TYPE_DEFAULT;
    settings->flags = ARG_FLAG_NONE;
    settings->prefix = NULL;
    settings->suffix = NULL;
    settings->delimiter = NULL;
    memset(settings->names, 0, names_length * sizeof(char*));

    return settings;
}


void  settings_free(Settings  *settings)
{
    if ( settings->names != NULL )
        free(settings->names);
    if ( settings->prefix != NULL && !(settings->flags & ARG_FLAG_LITERAL_PREFIX) )
        free(settings->prefix);
    if ( settings->suffix != NULL && !(settings->flags & ARG_FLAG_LITERAL_SUFFIX) )
        free(settings->suffix);
    if ( settings->delimiter != NULL && !(settings->flags & ARG_FLAG_LITERAL_DELIMITER) )
        free(settings->delimiter);

    free(settings);
}

/* vim: set ft=c sw=4 sts=4 et: */
