#ifndef RESOLVER_H
#define RESOLVER_H

#include "settings.h"

int  resolve_names(Settings  *settings);
int  resolve_adresses(Settings  *settings);

#endif /* RESOLVER_H */
/* vim: set ft=c sw=4 sts=4 et: */
