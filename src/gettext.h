/*
 * gettext.h  - Gettext macros.
 *
 * This file is part of gai project.
 *
 * gai is  free software: you  can redistribute it  and/or modify it  under the
 * terms of  the GNU General Public  License as published by  the Free Software
 * Foundation, either version  3 of the License, or (at  your option) any later
 * version.
 *
 * gai is  distributed in  the hope  that it  will be  useful, but  WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * gai. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef GETTEXT_H
#define GETTEXT_H

#include "config.h"
#ifdef GETTEXT
#include <libintl.h>

#define _(T)        gettext((T))
#define N_(T,P,N)   ngettext((T),(P),(N))
#else
#define _(T)        (T)
#define N_(T,P,N)   (T)
#endif /* GETTEXT */

#endif /* GETTEXT_H */
