*gai* is a simple domain names resolving tool, that uses `getaddrinfo()` and
`getnameinfo()` system calls, which means it honors NSS backends such as
`/etc/hosts` file and *nscd*. Unlike programs normally used in command line and
scripts: *host* and *dig*, *gai* does not query DNS servers directly, therefore
it should not be used as a DNS diagnostic tool.

#### Changelog

##### 0.9.5

  - Added `--delimiter` and `--literal-delimiter` options
  - Minor fixes

##### 0.9.4

  - Added `--literal-prefix` and `--literal-suffix` options
  - Portability fixes and cleanups

##### 0.9.3

  - Portability fixes for \*BSD and Solaris

##### 0.9.2

  - Added reverse resolution functionality

##### 0.9.1

  - Fixed SIGSEGV on \*BSD when local host name cannot be resolved
  - Fixed double error on IDN fail
  - Portability fixes for \*BSD and Solaris

##### 0.9.0

  - Initial version

#### Compilation and installation

Below is a list of additional software required or recommended for compilation:

  * `required` GNU autotools (aclocal, autoconf, automake)
  * `required` getopt (preferably with `getopt_long()`)
  * `required` gmake
  * `required` gzip (for man page compressing)
  * `optional` GNU Gettext with tools (xgettext, msginit, msgmerge, msgfmt)
  * `optional` libidn (**NOT** libidn2)

If *getopt* has no `getopt_long()` function, long options will not be available.
If *Gettext* is not present or disabled, error messages translations will not be
available. If *libidn* is not present or disabled, resolution of International
Domain Names will not be available.

To compile `gai`, clone it and execute the following commands:

```sh
gai-clone$ ./autoinit.sh
gai-clone$ ./configure
gai-clone$ gmake
...
gai-clone# gmake install
```

You may wish to alter some options before compiling. This can be done by running
`./configure` with arguments. Try `./configure --help` to get a list of
switches.

When recompiling, it is advisable to start the whole process anew. Resetting the
project to its initial state can be done with command:

```sh
gai-clone$ gmake squeky-clean
```

#### Portability

| System | Compiles? | Runs? | Tested? | Notes |
| :--- | :---: | :---: | :---: | :--- |
| GNU/Linux glibc6 | yes | yes | yes | none |
| FreeBSD 11 | yes | yes | yes | none |
| NetBSD 7 | yes | yes | yes | none |
| OpenBSD 6 | yes | yes | yes | Updated GCC is required |
| MacOSX | yes | yes | yes | IDN and Gettext were not tested |
| Oracle Solaris 11 | yes | yes | yes | IDN not available |
| MS Windows +7 | no | no | no need | Non-POSIX system, may work on CYGWIN |
| MS Windows 8+ | no | no | no need | Non-POSIX system, may work on CYGWIN, should work in "Windows Bash" - see "GNU/Linux" |
